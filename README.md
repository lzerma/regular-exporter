#Regular Export System

## Introduction

**This project shouldn't be a web project. If it is, something is quite wrong. :(**

The idea behind this project is organize and centralize every job that must be executed on CLI and the core 
of the application is over Zend Framework 2 and your structure is based on one single Module (RegularExports) 
and have Doctrine as default ORM.

There is a Command Line Interface default from Zend Framework 2 and a Slm module for integration with
 email sender like Mailchimp, Sendgrid, Amazon Elastic Mail and Mailgun (In use). 
  
The structure folder is pretty simple and most used on the community, let's see how it is:

 - rootPath
    - bin 
        + console (execute file) 
    - config
        + autoload (autoload files zf2)
    - data (cache and temp files)
    - module (Modules path)
        + RegularExports
            + config
            + src
                + RegularExports
                    + Controller
                    + Entity
                    + Repository
                    + Service
    - vendor (Third part libraries)
 - composer.json
 
 So as you can see, the structure is pretty simple and lean. Now we are going to see how the CLI routes were implemented.
 Inside of the folder rootPath -> module -> RegularExports -> config there are 2 files, we are focusing in one of them for this topic, 
 it will be the **console.router.routes.php** file. It's is responsible to store all console routes of the application. 
 
 The file consist in an multidimensional array where is defined the routes and the parameters that the route will receive. 
 Let's see how is the array:
 
 ```php
    return array(
      'console' => array(
         'router' => array(
             'routes' => array(
                 'my-first-route' => array(
                     'type'    => 'simple',
                     'options' => array(
                         'route'    => 'report:vro monthly <bodyTypes> [--email=]',
                         'defaults' => array(
                             'controller' => Controller\VroSourceController::class,
                             'action'     => 'monthly'
                         ),
                     ),
                 ),
             )
         )
     ),
```   
If you want more information on how improve this routes, add more parameters you can learn here: [Console routes and routing - ZF2](http://framework.zend.com/manual/current/en/modules/zend.console.routes.html)

## Installation

You must run the composer for the proper installation of the third parties libraries. 
```
php composer.phar update
```

## Third parties software
We're using third parties software and services to make this projects work.
The first and most important is [Zend Framework 2](https://github.com/zendframework/zf2), the second is also important (Doctrine 2)[https://github.com/doctrine/doctrine2]
but for make ZF2 and Doctrine2 work together without complications we use a service provider called [Doctrine ORM Module](https://github.com/doctrine/DoctrineORMModule).
And for last but not less important we are using [SLM Mail](https://github.com/juriansluiman/SlmMail) for manipulating emails as God. ;)

In fact for sending emails, we are using [Mailgun Service](https://mailgun.com), which we have a **free** subscription to send **10.000/month** emails. 