<?php
namespace RegularExports;

return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'my-first-route' => array(
                    'type'    => 'simple',
                    'options' => array(
                        'route'    => 'report:vro monthly <bodyTypes> [--email=]',
                        'defaults' => array(
                            'controller' => Controller\VroSourceController::class,
                            'action'     => 'monthly'
                        ),
                    ),
                ),
            )
        )
    ),
);
