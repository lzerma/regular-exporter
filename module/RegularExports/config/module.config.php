<?php
namespace RegularExports;

use RegularExports\Service\Mail;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

return array(
    'doctrine'        => array(
        'driver'        => array(
            'orm_default_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/RegularExports/Entity',
                ),
            ),
            'orm_default'        => array(
                'drivers' => array(
                    'RegularExports\Entity' => 'orm_default_driver'
                )
            ),
            'orm_nvdf'           => array(
                'drivers' => array(
                    'RegularExports\Entity' => 'orm_default_driver'
                )
            )
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories'          => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
            Mail::class  => function (ServiceManager $sm) {
                return new Mail($sm);
            },
        ),
    ),
    'controllers'     => array(
        'invokables' => array(
            Controller\VroSourceController::class => Controller\VroSourceController::class
        ),
    ),
);
