<?php
namespace RegularExports\Entity\NVDF;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vehicle
 *
 * @ORM\Table(name="nvdf")
 * @ORM\Entity(repositoryClass="RegularExports\Repository\NVDF\Vehicle", readOnly=true)
 *
 * @package RegularExports\Entity\NVDF
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @var null
     */
    private $reg = null;

    /**
     * @ORM\Column(type="string")
     */
    private $make = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $model = null;

    /**
     * @ORM\Column(type="string")
     */
    private $chassisNo = null;

    /**
     * @ORM\Column(type="string")
     */
    private $engineNo = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $engineCC = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $year = null;

    /**
     * @ORM\Column(type="string")
     */
    private $regDate = null;

    /**
     * @ORM\Column(type="string")
     */
    private $regDateIe = null;

    /**
     * @ORM\Column(type="date")
     */
    private $saleDate = null;

    /**
     * @ORM\Column(type="string")
     */
    private $body = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $gvw = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $uw = null;

    /**
     * @ORM\Column(type="string")
     */
    private $taxClass = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $noOfPrevOwners = null;

    /**
     * @ORM\Column(type="string")
     */
    private $prevReg = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $axles = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $fuel = null;

    /**
     * @ORM\Column(type="string")
     */
    private $colour = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $seats = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $doors = null;

    /**
     * @ORM\Column(type="date")
     */
    private $taxExpDate = null;

    /**
     * @ORM\Column(type="string")
     */
    private $nctCertNo = null;

    /**
     * @ORM\Column(type="date")
     */
    private $nctExpDate = null;

    /**
     * @ORM\Column(type="date")
     */
    private $nctPassDate = null;

    /**
     * @ORM\Column(type="date")
     */
    private $insuranceExpDate = null;

    /**
     * @ORM\Column(type="string")
     */
    private $vehStatus = null;

    /**
     * @ORM\Column(type="string")
     */
    private $vehRegStatus = null;

    /**
     * @ORM\Column(type="string")
     */
    private $importedFrom = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $wheelbase = null;

    /**
     * @ORM\Column(type="string")
     */
    private $statcode = null;

    /**
     * @ORM\Column(type="string")
     */
    private $modelDesc = null;

    /**
     * @ORM\Column(type="string")
     */
    private $transmission = null;

    /**
     * @ORM\Column(type="string")
     */
    private $vlcNo = null;

    /**
     * @ORM\Column(type="string")
     */
    private $ownerCategory = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $co2 = null;

    /**
     * @ORM\Column(type="date")
     */
    private $crwExpDate = null;

    /**
     * @ORM\Column(type="date")
     */
    private $lastSeenOnNvdf = null;

    /**
     * @ORM\Column(type="date")
     */
    private $updated_at = null;

    /**
     * @ORM\Column(type="date")
     */
    private $inserted_at = null;

    /**
     * @return mixed
     */
    public function getReg()
    {
        return $this->reg;
    }

    /**
     * @param mixed $reg
     */
    public function setReg($reg)
    {
        $this->reg = $reg;
    }

    /**
     * @return mixed
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param mixed $make
     */
    public function setMake($make)
    {
        $this->make = $make;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getChassisNo()
    {
        return $this->chassisNo;
    }

    /**
     * @param mixed $chassisNo
     */
    public function setChassisNo($chassisNo)
    {
        $this->chassisNo = $chassisNo;
    }

    /**
     * @return mixed
     */
    public function getEngineNo()
    {
        return $this->engineNo;
    }

    /**
     * @param mixed $engineNo
     */
    public function setEngineNo($engineNo)
    {
        $this->engineNo = $engineNo;
    }

    /**
     * @return mixed
     */
    public function getEngineCC()
    {
        return $this->engineCC;
    }

    /**
     * @param mixed $engineCC
     */
    public function setEngineCC($engineCC)
    {
        $this->engineCC = $engineCC;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * @param mixed $regDate
     */
    public function setRegDate($regDate)
    {
        $this->regDate = $regDate;
    }

    /**
     * @return mixed
     */
    public function getRegDateIe()
    {
        return $this->regDateIe;
    }

    /**
     * @param mixed $regDateIe
     */
    public function setRegDateIe($regDateIe)
    {
        $this->regDateIe = $regDateIe;
    }

    /**
     * @return mixed
     */
    public function getSaleDate()
    {
        return $this->saleDate;
    }

    /**
     * @param mixed $saleDate
     */
    public function setSaleDate($saleDate)
    {
        $this->saleDate = $saleDate;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getGvw()
    {
        return $this->gvw;
    }

    /**
     * @param mixed $gvw
     */
    public function setGvw($gvw)
    {
        $this->gvw = $gvw;
    }

    /**
     * @return mixed
     */
    public function getUw()
    {
        return $this->uw;
    }

    /**
     * @param mixed $uw
     */
    public function setUw($uw)
    {
        $this->uw = $uw;
    }

    /**
     * @return mixed
     */
    public function getTaxClass()
    {
        return $this->taxClass;
    }

    /**
     * @param mixed $taxClass
     */
    public function setTaxClass($taxClass)
    {
        $this->taxClass = $taxClass;
    }

    /**
     * @return mixed
     */
    public function getNoOfPrevOwners()
    {
        return $this->noOfPrevOwners;
    }

    /**
     * @param mixed $noOfPrevOwners
     */
    public function setNoOfPrevOwners($noOfPrevOwners)
    {
        $this->noOfPrevOwners = $noOfPrevOwners;
    }

    /**
     * @return mixed
     */
    public function getPrevReg()
    {
        return $this->prevReg;
    }

    /**
     * @param mixed $prevReg
     */
    public function setPrevReg($prevReg)
    {
        $this->prevReg = $prevReg;
    }

    /**
     * @return mixed
     */
    public function getAxles()
    {
        return $this->axles;
    }

    /**
     * @param mixed $axles
     */
    public function setAxles($axles)
    {
        $this->axles = $axles;
    }

    /**
     * @return mixed
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param mixed $fuel
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;
    }

    /**
     * @return mixed
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * @param mixed $colour
     */
    public function setColour($colour)
    {
        $this->colour = $colour;
    }

    /**
     * @return mixed
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param mixed $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return mixed
     */
    public function getDoors()
    {
        return $this->doors;
    }

    /**
     * @param mixed $doors
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;
    }

    /**
     * @return mixed
     */
    public function getTaxExpDate()
    {
        return $this->taxExpDate;
    }

    /**
     * @param mixed $taxExpDate
     */
    public function setTaxExpDate($taxExpDate)
    {
        $this->taxExpDate = $taxExpDate;
    }

    /**
     * @return mixed
     */
    public function getNctCertNo()
    {
        return $this->nctCertNo;
    }

    /**
     * @param mixed $nctCertNo
     */
    public function setNctCertNo($nctCertNo)
    {
        $this->nctCertNo = $nctCertNo;
    }

    /**
     * @return mixed
     */
    public function getNctExpDate()
    {
        return $this->nctExpDate;
    }

    /**
     * @param mixed $nctExpDate
     */
    public function setNctExpDate($nctExpDate)
    {
        $this->nctExpDate = $nctExpDate;
    }

    /**
     * @return mixed
     */
    public function getNctPassDate()
    {
        return $this->nctPassDate;
    }

    /**
     * @param mixed $nctPassDate
     */
    public function setNctPassDate($nctPassDate)
    {
        $this->nctPassDate = $nctPassDate;
    }

    /**
     * @return mixed
     */
    public function getInsuranceExpDate()
    {
        return $this->insuranceExpDate;
    }

    /**
     * @param mixed $insuranceExpDate
     */
    public function setInsuranceExpDate($insuranceExpDate)
    {
        $this->insuranceExpDate = $insuranceExpDate;
    }

    /**
     * @return mixed
     */
    public function getVehStatus()
    {
        return $this->vehStatus;
    }

    /**
     * @param mixed $vehStatus
     */
    public function setVehStatus($vehStatus)
    {
        $this->vehStatus = $vehStatus;
    }

    /**
     * @return mixed
     */
    public function getVehRegStatus()
    {
        return $this->vehRegStatus;
    }

    /**
     * @param mixed $vehRegStatus
     */
    public function setVehRegStatus($vehRegStatus)
    {
        $this->vehRegStatus = $vehRegStatus;
    }

    /**
     * @return mixed
     */
    public function getImportedFrom()
    {
        return $this->importedFrom;
    }

    /**
     * @param mixed $importedFrom
     */
    public function setImportedFrom($importedFrom)
    {
        $this->importedFrom = $importedFrom;
    }

    /**
     * @return mixed
     */
    public function getWheelbase()
    {
        return $this->wheelbase;
    }

    /**
     * @param mixed $wheelbase
     */
    public function setWheelbase($wheelbase)
    {
        $this->wheelbase = $wheelbase;
    }

    /**
     * @return mixed
     */
    public function getStatcode()
    {
        return $this->statcode;
    }

    /**
     * @param mixed $statcode
     */
    public function setStatcode($statcode)
    {
        $this->statcode = $statcode;
    }

    /**
     * @return mixed
     */
    public function getModelDesc()
    {
        return $this->modelDesc;
    }

    /**
     * @param mixed $modelDesc
     */
    public function setModelDesc($modelDesc)
    {
        $this->modelDesc = $modelDesc;
    }

    /**
     * @return mixed
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * @param mixed $transmission
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;
    }

    /**
     * @return mixed
     */
    public function getVlcNo()
    {
        return $this->vlcNo;
    }

    /**
     * @param mixed $vlcNo
     */
    public function setVlcNo($vlcNo)
    {
        $this->vlcNo = $vlcNo;
    }

    /**
     * @return mixed
     */
    public function getOwnerCategory()
    {
        return $this->ownerCategory;
    }

    /**
     * @param mixed $ownerCategory
     */
    public function setOwnerCategory($ownerCategory)
    {
        $this->ownerCategory = $ownerCategory;
    }

    /**
     * @return mixed
     */
    public function getCo2()
    {
        return $this->co2;
    }

    /**
     * @param mixed $co2
     */
    public function setCo2($co2)
    {
        $this->co2 = $co2;
    }

    /**
     * @return mixed
     */
    public function getCrwExpDate()
    {
        return $this->crwExpDate;
    }

    /**
     * @param mixed $crwExpDate
     */
    public function setCrwExpDate($crwExpDate)
    {
        $this->crwExpDate = $crwExpDate;
    }

    /**
     * @return mixed
     */
    public function getLastSeenOnNvdf()
    {
        return $this->lastSeenOnNvdf;
    }

    /**
     * @param mixed $lastSeenOnNvdf
     */
    public function setLastSeenOnNvdf($lastSeenOnNvdf)
    {
        $this->lastSeenOnNvdf = $lastSeenOnNvdf;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getInsertedAt()
    {
        return $this->inserted_at;
    }

    /**
     * @param mixed $inserted_at
     */
    public function setInsertedAt($inserted_at)
    {
        $this->inserted_at = $inserted_at;
    }
}