<?php
namespace RegularExports\Entity\VRO;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ImportLog
 *
 * @ORM\Table(name="import_log")
 * @ORM\Entity(readOnly=true)
 *
 * @package RegularExports\Entity\VRO
 */
class ImportLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id = null;

    /**
     * @ORM\Column(type="string")
     */
    private $file_name = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $file_lines = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $imports = null;

    /**
     * @ORM\Column(type="integer")
     */
    private $duplicates = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private $successful = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->file_name;
    }

    /**
     * @param mixed $file_name
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return mixed
     */
    public function getFileLines()
    {
        return $this->file_lines;
    }

    /**
     * @param mixed $file_lines
     */
    public function setFileLines($file_lines)
    {
        $this->file_lines = $file_lines;
    }

    /**
     * @return mixed
     */
    public function getImports()
    {
        return $this->imports;
    }

    /**
     * @param mixed $imports
     */
    public function setImports($imports)
    {
        $this->imports = $imports;
    }

    /**
     * @return mixed
     */
    public function getDuplicates()
    {
        return $this->duplicates;
    }

    /**
     * @param mixed $duplicates
     */
    public function setDuplicates($duplicates)
    {
        $this->duplicates = $duplicates;
    }

    /**
     * @return mixed
     */
    public function getSuccessful()
    {
        return $this->successful;
    }

    /**
     * @param mixed $successful
     */
    public function setSuccessful($successful)
    {
        $this->successful = $successful;
    }
}