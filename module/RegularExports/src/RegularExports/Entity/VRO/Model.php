<?php
namespace RegularExports\Entity\VRO;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Model
 *
 * @ORM\Table(name="model")
 * @ORM\Entity(readOnly=true)
 *
 * @package RegularExports\Entity\VRO
 */
class Model
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id = null;

    /**
     * @ORM\Column(type="string")
     */
    private $name = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
}