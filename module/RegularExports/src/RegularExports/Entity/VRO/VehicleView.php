<?php
namespace RegularExports\Entity\VRO;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VehicleView
 *
 * @ORM\Table(name="vehicle_view")
 * @ORM\Entity(repositoryClass="RegularExports\Repository\VRO\VehicleView", readOnly=true)
 *
 * @package RegularExports\Entity\VRO
 */
class VehicleView
{

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $make_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $model_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $version_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $cleansed_make_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $cleansed_model_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $cleansed_version_name = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $cleansed_version_id = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $cleansed_make_id = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $cleansed_model_id = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $body_type_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $colour_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $engine_type_name = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $transmission_name = null;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @var null
     */
    private $id = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $reg = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $reg_vro = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $vin = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $vin_vro = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $county = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\Make")
     * @ORM\JoinColumn(name="make_id", referencedColumnName="id")
     */
    private $make = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\Model")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    private $model = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\Version")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $version = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $reg_date = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $stat_code = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $search_code = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $category = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $category_vro = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\BodyType")
     * @ORM\JoinColumn(name="body_type_id", referencedColumnName="id")
     */
    private $body_type = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $engine_cc = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $engine_no = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\EngineType")
     * @ORM\JoinColumn(name="engine_type_id", referencedColumnName="id")
     */
    private $engine_type = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $colour_code = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $unladen_weight = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $gvw = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $gcw = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $co2_emissions = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $eu_cat = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $doors = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $seats = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $windows = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\Transmission")
     * @ORM\JoinColumn(name="transmission_id", referencedColumnName="id")
     */
    private $transmission = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $country_of_origin_id = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $net_power = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $bhp = null;

    /**
     * @ORM\Column(type="integer")
     * @var null
     */
    private $wheelbase = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $status = null;

    /**
     * @ORM\Column(type="string")
     * @var null
     */
    private $first_owner_type = null;

    /**
     * @ORM\ManyToOne(targetEntity="RegularExports\Entity\VRO\ImportLog")
     * @ORM\JoinColumn(name="import_log_id", referencedColumnName="id")
     */
    private $import_log = null;

    /**
     * @ORM\Column(type="datetime")
     * @var null
     */
    private $updated_at = null;

    /**
     * @ORM\Column(type="datetime")
     * @var null
     */
    private $created_at = null;

    /**
     * @return null
     */
    public function getMakeName()
    {
        return $this->make_name;
    }

    /**
     * @param null $make_name
     */
    public function setMakeName($make_name)
    {
        $this->make_name = $make_name;
    }

    /**
     * @return null
     */
    public function getModelName()
    {
        return $this->model_name;
    }

    /**
     * @param null $model_name
     */
    public function setModelName($model_name)
    {
        $this->model_name = $model_name;
    }

    /**
     * @return null
     */
    public function getVersionName()
    {
        return $this->version_name;
    }

    /**
     * @param null $version_name
     */
    public function setVersionName($version_name)
    {
        $this->version_name = $version_name;
    }

    /**
     * @return null
     */
    public function getCleansedMakeName()
    {
        return $this->cleansed_make_name;
    }

    /**
     * @param null $cleansed_make_name
     */
    public function setCleansedMakeName($cleansed_make_name)
    {
        $this->cleansed_make_name = $cleansed_make_name;
    }

    /**
     * @return null
     */
    public function getCleansedModelName()
    {
        return $this->cleansed_model_name;
    }

    /**
     * @param null $cleansed_model_name
     */
    public function setCleansedModelName($cleansed_model_name)
    {
        $this->cleansed_model_name = $cleansed_model_name;
    }

    /**
     * @return null
     */
    public function getCleansedVersionName()
    {
        return $this->cleansed_version_name;
    }

    /**
     * @param null $cleansed_version_name
     */
    public function setCleansedVersionName($cleansed_version_name)
    {
        $this->cleansed_version_name = $cleansed_version_name;
    }

    /**
     * @return null
     */
    public function getCleansedVersionId()
    {
        return $this->cleansed_version_id;
    }

    /**
     * @param null $cleansed_version_id
     */
    public function setCleansedVersionId($cleansed_version_id)
    {
        $this->cleansed_version_id = $cleansed_version_id;
    }

    /**
     * @return null
     */
    public function getCleansedMakeId()
    {
        return $this->cleansed_make_id;
    }

    /**
     * @param null $cleansed_make_id
     */
    public function setCleansedMakeId($cleansed_make_id)
    {
        $this->cleansed_make_id = $cleansed_make_id;
    }

    /**
     * @return null
     */
    public function getCleansedModelId()
    {
        return $this->cleansed_model_id;
    }

    /**
     * @param null $cleansed_model_id
     */
    public function setCleansedModelId($cleansed_model_id)
    {
        $this->cleansed_model_id = $cleansed_model_id;
    }

    /**
     * @return null
     */
    public function getBodyTypeName()
    {
        return $this->body_type_name;
    }

    /**
     * @param null $body_type_name
     */
    public function setBodyTypeName($body_type_name)
    {
        $this->body_type_name = $body_type_name;
    }

    /**
     * @return null
     */
    public function getColourName()
    {
        return $this->colour_name;
    }

    /**
     * @param null $colour_name
     */
    public function setColourName($colour_name)
    {
        $this->colour_name = $colour_name;
    }

    /**
     * @return null
     */
    public function getEngineTypeName()
    {
        return $this->engine_type_name;
    }

    /**
     * @param null $engine_type_name
     */
    public function setEngineTypeName($engine_type_name)
    {
        $this->engine_type_name = $engine_type_name;
    }

    /**
     * @return null
     */
    public function getTransmissionName()
    {
        return $this->transmission_name;
    }

    /**
     * @param null $transmission_name
     */
    public function setTransmissionName($transmission_name)
    {
        $this->transmission_name = $transmission_name;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getReg()
    {
        return $this->reg;
    }

    /**
     * @param null $reg
     */
    public function setReg($reg)
    {
        $this->reg = $reg;
    }

    /**
     * @return null
     */
    public function getRegVro()
    {
        return $this->reg_vro;
    }

    /**
     * @param null $reg_vro
     */
    public function setRegVro($reg_vro)
    {
        $this->reg_vro = $reg_vro;
    }

    /**
     * @return null
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * @param null $vin
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
    }

    /**
     * @return null
     */
    public function getVinVro()
    {
        return $this->vin_vro;
    }

    /**
     * @param null $vin_vro
     */
    public function setVinVro($vin_vro)
    {
        $this->vin_vro = $vin_vro;
    }

    /**
     * @return null
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param null $county
     */
    public function setCounty($county)
    {
        $this->county = $county;
    }

    /**
     * @return null
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * @param null $make
     */
    public function setMake($make)
    {
        $this->make = $make;
    }

    /**
     * @return null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param null $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return null
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param null $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return null
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param null $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return null
     */
    public function getStatCode()
    {
        return $this->stat_code;
    }

    /**
     * @param null $stat_code
     */
    public function setStatCode($stat_code)
    {
        $this->stat_code = $stat_code;
    }

    /**
     * @return null
     */
    public function getSearchCode()
    {
        return $this->search_code;
    }

    /**
     * @param null $search_code
     */
    public function setSearchCode($search_code)
    {
        $this->search_code = $search_code;
    }

    /**
     * @return null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param null $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return null
     */
    public function getCategoryVro()
    {
        return $this->category_vro;
    }

    /**
     * @param null $category_vro
     */
    public function setCategoryVro($category_vro)
    {
        $this->category_vro = $category_vro;
    }

    /**
     * @return null
     */
    public function getBodyType()
    {
        return $this->body_type;
    }

    /**
     * @param null $body_type
     */
    public function setBodyType($body_type)
    {
        $this->body_type = $body_type;
    }

    /**
     * @return null
     */
    public function getEngineCc()
    {
        return $this->engine_cc;
    }

    /**
     * @param null $engine_cc
     */
    public function setEngineCc($engine_cc)
    {
        $this->engine_cc = $engine_cc;
    }

    /**
     * @return null
     */
    public function getEngineNo()
    {
        return $this->engine_no;
    }

    /**
     * @param null $engine_no
     */
    public function setEngineNo($engine_no)
    {
        $this->engine_no = $engine_no;
    }

    /**
     * @return null
     */
    public function getEngineType()
    {
        return $this->engine_type;
    }

    /**
     * @param null $engine_type
     */
    public function setEngineType($engine_type)
    {
        $this->engine_type = $engine_type;
    }

    /**
     * @return null
     */
    public function getColourCode()
    {
        return $this->colour_code;
    }

    /**
     * @param null $colour_code
     */
    public function setColourCode($colour_code)
    {
        $this->colour_code = $colour_code;
    }

    /**
     * @return null
     */
    public function getUnladenWeight()
    {
        return $this->unladen_weight;
    }

    /**
     * @param null $unladen_weight
     */
    public function setUnladenWeight($unladen_weight)
    {
        $this->unladen_weight = $unladen_weight;
    }

    /**
     * @return null
     */
    public function getGvw()
    {
        return $this->gvw;
    }

    /**
     * @param null $gvw
     */
    public function setGvw($gvw)
    {
        $this->gvw = $gvw;
    }

    /**
     * @return null
     */
    public function getGcw()
    {
        return $this->gcw;
    }

    /**
     * @param null $gcw
     */
    public function setGcw($gcw)
    {
        $this->gcw = $gcw;
    }

    /**
     * @return null
     */
    public function getCo2Emissions()
    {
        return $this->co2_emissions;
    }

    /**
     * @param null $co2_emissions
     */
    public function setCo2Emissions($co2_emissions)
    {
        $this->co2_emissions = $co2_emissions;
    }

    /**
     * @return null
     */
    public function getEuCat()
    {
        return $this->eu_cat;
    }

    /**
     * @param null $eu_cat
     */
    public function setEuCat($eu_cat)
    {
        $this->eu_cat = $eu_cat;
    }

    /**
     * @return null
     */
    public function getDoors()
    {
        return $this->doors;
    }

    /**
     * @param null $doors
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;
    }

    /**
     * @return null
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param null $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return null
     */
    public function getWindows()
    {
        return $this->windows;
    }

    /**
     * @param null $windows
     */
    public function setWindows($windows)
    {
        $this->windows = $windows;
    }

    /**
     * @return null
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * @param null $transmission
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;
    }

    /**
     * @return null
     */
    public function getCountryOfOriginId()
    {
        return $this->country_of_origin_id;
    }

    /**
     * @param null $country_of_origin_id
     */
    public function setCountryOfOriginId($country_of_origin_id)
    {
        $this->country_of_origin_id = $country_of_origin_id;
    }

    /**
     * @return null
     */
    public function getNetPower()
    {
        return $this->net_power;
    }

    /**
     * @param null $net_power
     */
    public function setNetPower($net_power)
    {
        $this->net_power = $net_power;
    }

    /**
     * @return null
     */
    public function getBhp()
    {
        return $this->bhp;
    }

    /**
     * @param null $bhp
     */
    public function setBhp($bhp)
    {
        $this->bhp = $bhp;
    }

    /**
     * @return null
     */
    public function getWheelbase()
    {
        return $this->wheelbase;
    }

    /**
     * @param null $wheelbase
     */
    public function setWheelbase($wheelbase)
    {
        $this->wheelbase = $wheelbase;
    }

    /**
     * @return null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param null $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return null
     */
    public function getFirstOwnerType()
    {
        return $this->first_owner_type;
    }

    /**
     * @param null $first_owner_type
     */
    public function setFirstOwnerType($first_owner_type)
    {
        $this->first_owner_type = $first_owner_type;
    }

    /**
     * @return null
     */
    public function getImportLog()
    {
        return $this->import_log;
    }

    /**
     * @param null $import_log
     */
    public function setImportLog($import_log)
    {
        $this->import_log = $import_log;
    }

    /**
     * @return null
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param null $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return null
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param null $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $a = array();
        foreach ($this as $prop => $value) {
            if (!is_object($value)) {
                $a[$prop] = (is_null($value)) ? 'N/A' : $value;
            } else {
                if (method_exists($value, 'getName')) {
                    $a[$prop] = $value->getName();
                } elseif (method_exists($value, 'getFileName')) {
                    $a[$prop] = $value->getFileName();
                }
            }
        }
        return $a;
    }
}