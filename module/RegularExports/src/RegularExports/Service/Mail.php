<?php
namespace RegularExports\Service;

use SlmMail\Mail\Message\Mailgun;
use SlmMail\Mail\Message\Postmark;
use Zend\Http\Client;
use Zend\ServiceManager\ServiceManager;

/**
 * Class Mail
 *
 * @package RegularExports\Service
 */
class Mail
{
    /**
     * @var ServiceManager
     */
    private $serviceManager = null;

    /**
     * @var Mailgun
     */
    private $message = null;

    public function __construct(ServiceManager $sm)
    {
        $this->setServiceManager($sm);
    }

    /**
     * @param array $recipients
     * @param       $subject
     *
     * @return $this
     */
    public function prepare(array $recipients, $subject)
    {
        $this->setMessage(new Postmark());
        $this->getMessage()->setTag('monthly-report');
        $this->getMessage()->setFrom('report@motorcheck.ie');
        $this->getMessage()->addTo($recipients);
        $this->getMessage()->setSubject($subject);
        return $this;
    }

    /**
     * @return bool
     */
    public function send()
    {
        try {
            $transport = $this->getServiceManager()->get('SlmMail\Service\PostmarkService');
            $clientConfig = array(
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'curloptions' => array(
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_SSL_VERIFYPEER => FALSE
                ),
            );
            $client = new Client();
            $client->setOptions($clientConfig);
            $transport->setClient($client);
            $transport->send($this->getMessage());
            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $name
     * @param $mimetype
     * @param $path
     *
     * @return $this
     */
    public function setAttachement($name, $mimetype, $path)
    {
        $attachment = new \Zend\Mime\Part(fopen($path, 'r'));
        $attachment->setType($mimetype);
        $attachment->setFileName($name);
        $html = '<h2>Report generated automatically, please do not reply.</h2>';
        $html .= '<p>The report for vro database was sucessfully generated. Please see the file attached.</p>';
        $htmlBody = new \Zend\Mime\Part($html);
        $htmlBody->setType('text/html');

        $body = new \Zend\Mime\Message();
        $body->setParts(array($attachment, $htmlBody));
        $this->getMessage()->setBody($body);
        return $this;
    }

    /**
     * @return Mailgun
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Mailgun $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
}