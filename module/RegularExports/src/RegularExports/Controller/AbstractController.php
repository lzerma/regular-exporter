<?php
namespace RegularExports\Controller;

use Doctrine\ORM\EntityManager;
use Zend\Console\Console;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\View\Model\ViewModel;

/**
 * Class AbstractController
 *
 * @package RegularExports\Controller
 */
class AbstractController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * @return EntityManager
     */
    public function getEm($type = 'default')
    {
        try {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_'.$type);
            if ($this->em instanceof EntityManager) {
                return $this->em;
            } else {
                throw new ServiceNotFoundException('Doctrine Entity Manager not found.');
            }
        }
        catch(\AbstractDriverException $e) {
            throw new ServiceNotFoundException('Doctrine Entity Manager not found.');
        }
    }

    /**
     * @param            $mixed
     * @param bool|false $title
     * @param bool|false $return
     *
     * @return string
     */
    protected function output($mixed, $title = false, $color = null, $bgColor = null, $return = false)
    {
        $console = Console::getInstance();
        $date = date('Y-m-d H:i:s');
        if ($return) {
            if (!$title) {
                return "[$date] - " . $mixed . PHP_EOL;
            } else {
                return "[$date] ----- " . $mixed . ' -----' . PHP_EOL;
            }
        } else {
            if (!$title) {
                $console->writeLine("[$date] - " . $mixed, $color, $bgColor);
            } else {
                $console->writeLine("[$date] ----- " . $mixed . ' -----', $color, $bgColor);
            }
        }
    }
}
