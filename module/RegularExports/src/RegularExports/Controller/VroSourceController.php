<?php
namespace RegularExports\Controller;

use RegularExports\Entity\NVDF\Vehicle;
use RegularExports\Entity\VRO\VehicleView;
use RegularExports\Service\Mail;
use Zend\Console\ColorInterface;
use Zend\Console\Exception\InvalidArgumentException;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


/**
 * Class VroSourceController
 *
 * @package RegularExports\Controller
 */
class VroSourceController extends AbstractController
{
    /**
     * @return ViewModel
     */
    public function monthlyAction()
    {
        $lastRun = $this->lastRun();
        $this->output(
            'Monthly regular report started', false, ColorInterface::BLACK, ColorInterface::CYAN
        );

        $vehicleNvdfRepo = $this->getEm('nvdf')->getRepository(Vehicle::class);
        $lastProcessedDate = new \DateTime($vehicleNvdfRepo->getLastProcessedDate()['date']);
        $today = new \DateTime();

        if ($lastRun->format('Ym') == $today->format('Ym')) {
            $this->output(
                'This process already ran.', false, ColorInterface::BLACK,
                ColorInterface::BLUE
            );
            return false;
        }

        $lastMonth = clone $today;
        $lastMonth->sub(new \DateInterval('P1M'));

        if ($lastProcessedDate->format('Ym') <= $lastMonth->format('Ym')) {
            $this->output(
                'NVDF didn\'t run yet for the old month. Wait until then.', false, ColorInterface::BLACK,
                ColorInterface::YELLOW
            );
            return false;
        }

        $request = $this->getRequest();
        // Get user email from console and check if the user used --verbose or -v flag
        $bodyTypes = explode(',', $request->getParam('bodyTypes'));
        $recipients = explode(',', $request->getParam('email'));

        if (count($bodyTypes) == 0 && $recipients == 0) {
            throw new InvalidArgumentException('You must pass bodyTypes and email separeted by commas.');
        }

        $vehicleViewRepo = $this->getEm()->getRepository(VehicleView::class);

        $this->output(
            'Fetching versions for the following body types: [' . implode(',', $bodyTypes) . ']
            and month: ' . $lastMonth->format('Y-m'), false,
            ColorInterface::LIGHT_GREEN
        );
        $versions = $vehicleViewRepo->getLastMonth($bodyTypes, $today);
        $this->output(
            'Total of ' . count($versions) . ' versions was fetched', false, ColorInterface::LIGHT_GREEN
        );

        $this->output('Parsing CSV', false, ColorInterface::LIGHT_YELLOW);
        $fileId = $this->createCSV($versions);
        $this->output('Temporary file: ' . $fileId, false, ColorInterface::LIGHT_GREEN);

        $this->output('Preparing email to send', false, ColorInterface::LIGHT_YELLOW);

        $subject = 'New VRO Monthly report generated for ' . $lastMonth->format('Y-m');

        $this->getServiceLocator()->get(Mail::class)->prepare($recipients, $subject);

        $this->output(
            'To following address: [' . implode(', ', $recipients) . ']', false, ColorInterface::LIGHT_GREEN
        );

        $this->getServiceLocator()->get(Mail::class)->setAttachement(
            date('Y-m-d') . '-vro-report.csv', 'text/csv', $fileId
        );

        $ret = $this->getServiceLocator()->get(Mail::class)->send();
        if ($ret) {
            $this->output(
                'Email was sent with success.', false, ColorInterface::BLACK, ColorInterface::GREEN
            );
            $this->lastRun($today->format('Y-m-d'));
        } else {
            $this->output(
                'Email wasn\'t sent with success.', false, ColorInterface::BLACK, ColorInterface::RED
            );
        }

        return true;
    }

    /**
     * @param $versions
     *
     * @return string
     */
    private function createCSV($versions)
    {
        $id = tempnam("/tmp", uniqid('vro_' . date('Y-m-d')));
        $file = fopen($id, 'w');
        $columns = false;
        foreach ($versions as $version) {
            if (!$columns) {
                $columns = implode(', ', array_keys($version));
                fwrite($file, $columns);
                fwrite($file, PHP_EOL);
            }

            $version = array_map(
                function ($item) {
                    if ($item instanceof \DateTime) {
                        return $item->format('Y-m-d');
                    }

                    return $item;
                }, $version
            );

            $values = implode(', ', array_values($version));
            fwrite($file, $values);
            fwrite($file, PHP_EOL);
        }
        fclose($file);
        return $id;
    }

    /**
     * @param bool|false $lastRun
     *
     * @return \DateTime
     */
    private function lastRun($lastRun = false)
    {
        $filepath = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;
        $filename = str_replace('\\', '_', __CLASS__) . '.lock';
        if ($lastRun == false) {
            if (file_exists($filepath . $filename)) {
                return new \DateTime(file_get_contents($filepath . $filename));
            } else {
                return new \DateTime();
            }
        } else {
            file_put_contents($filepath . $filename, $lastRun . PHP_EOL);
        }
    }
}
