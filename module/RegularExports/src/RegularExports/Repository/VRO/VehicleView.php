<?php
namespace RegularExports\Repository\VRO;

use Doctrine\ORM\EntityRepository;

/**
 * Class VehicleView
 *
 * @package src\RegularExports\Repository\VRO
 */
class VehicleView extends EntityRepository
{
    /**
     * @param $bodyTypesId
     *
     * @return array
     */
    public function getLastMonth(array $bodyTypesId, \DateTime $date)
    {
        $lastMonth = clone $date;
        $interval = new \DateInterval('P1M');
        $lastMonth->sub($interval);

        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select(
            'v.vin, v.make_name, v.model_name, v.version_name, v.reg_date, v.stat_code, v.search_code,
            v.category, v.body_type_name, v.engine_cc, v.engine_no, v.engine_type_name, v.colour_name,
            v.co2_emissions, v.eu_cat, v.doors, v.transmission_name, v.county, v.bhp, v.status'
        )->from(\RegularExports\Entity\VRO\VehicleView::class, 'v');
        $query->andWhere('v.reg_date BETWEEN :last_month AND :actual_month')
            ->setParameter('last_month', $lastMonth->format('Y-m-01'))
            ->setParameter('actual_month', $lastMonth->format('Y-m-t'));

        $inBodyTypes = implode(',', $bodyTypesId);
        $query->andWhere("v.body_type IN ({$inBodyTypes})");
        $query->addOrderBy('v.reg_date', 'ASC');

        return $query->getQuery()->getArrayResult();
    }
}