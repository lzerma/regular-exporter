<?php
namespace RegularExports\Repository\NVDF;

use Doctrine\ORM\EntityRepository;

/**
 * Class Vehicle
 *
 * @package src\RegularExports\Repository\NVDF
 */
class Vehicle extends EntityRepository
{
    /**
     * @return array
     */
    public function getLastProcessedDate()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('MAX(v.lastSeenOnNvdf) as date')->from(
            \RegularExports\Entity\NVDF\Vehicle::class, 'v'
        );
        return $qb->getQuery()->getOneOrNullResult();
    }
}