<?php
namespace RegularExports;

use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements ConsoleBannerProviderInterface
{
    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    /**
     * This method is defined in ConsoleBannerProviderInterface
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return 'RegularExports Console Management';
    }

    /**
     * @param AdapterInterface $console
     *
     * @return array
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'report:vro monthly <bodyTypes> [--email=]' => 'Generate a report based on the last month for specific body types and send to a specific recipient',
            array('<bodyTypes>', 'body types id', 'A list of body types id to filter on the report.'),
            array('--email=EMAIL',     'Email of the user to find'),
        );
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $config = array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/console.router.routes.php'
        );
        return $config;
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
